<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\QCDocument;
use App\Models\QualityControl;
use \Unisharp\FileApi\FileApi;
use App\Models\User;
class QCDocumentController extends Controller
{   

    public function index($qc_id){
        $fa = new FileApi('images/quality-control');
        $photo= QCDocument::with([
                'author'=> function($query){
                    $query->select('id','name');
                },
            ])->where('quality_controls_id',$qc_id)->get();

        // dd($fa->getPath($photo->filename));
        return response()->json(['result'=>$photo],202);

    }

    public function store(Request $request, $qc_id){
    	$document= new QCDocument();
    	// $qc= new FileApi('/images/qc/lantai-45/ruang-101/divisi');
    	$qcdocument= new FileApi('/images/quality-control');

    	$file=$qcdocument->save($request->file('filename'));    	
    	$document->users_id=$request->user()->id;
    	$document->title=$request->input('title');
    	$document->filename=$file;
    	$document->quality_controls_id=$qc_id;
        $document->content=$request->input('content');

    	$document->save();

    	// $filename=$qc->getResponse($file);

    	return response()->json(['message'=>'berhasil menambah document'],201);

    	
    }

    public function show(Request $request, $id){
        $document = QCDocument::with([
                'author'=>function($query){
                    $query->select('id','name');
                }
            ])->where('id',$id)->first();    

       
        return response()->json($document);
    }

    
}
