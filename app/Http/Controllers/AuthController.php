<?php

namespace App\Http\Controllers;

//jwt auth
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\User;

class AuthController extends Controller
{
	public function signup(Request $request){
		$this->validate($request , [
			'name'=>'required',
			'username'=>'required|unique:users',
			'email'=>'required|unique:users',
			'password'=>'required',

			]);



		$user=User::create([
			'name'=>$request->json('name'),
			'username'=>$request->json('username'),
			'email'=>$request->json('email'),
			'password'=>bcrypt($request->json('password')),
			]);

		return $user;
	}

	public function signin(Request $request){
		$this->validate($request , [
			'username'=>'required',
			'password'=>'required',

			]);

		$credentials=$request->only('username','password');

		try{
			if(!$token=JWTAuth::attempt($credentials)){
				return response()->json(['error'=>'invalid_credentials'],401);
			}
		}catch(JWTException $e){
			return response()->json(['error'=>'could_not_create_token'],500);
		}

		return response()->json([
				'user_id'=>$request->user()->id,
				'token'=>$token,
			]);
	}

	public function getToken(){
		$token= JWTAuth::getToken();

		if(!$token){
			return response()->json(['message'=>'Token is invalid'],401);
		}

		try{
			$refreshedToken= JWTAuth::refresh($token);
		}catch(JWTException $ex){
			response()->json(['message'=>'Something went wrong'],402);
		}

		return response()->json(['token'=>$refreshedToken],202);
	}
}
