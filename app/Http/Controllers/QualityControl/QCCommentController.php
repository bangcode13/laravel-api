<?php

namespace App\Http\Controllers\QualityControl;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\QCComment;

class QCCommentController extends Controller
{	

	public function index($id){
		$comments= QCComment::select('content','user_id','created_at')
				->with([
				'author'=> function($query){
					$query->select('id','name');
				},
			])->where('qc_documents_id',$id)->orderBy('id','desc')->get();

		return response()->json(['result'=>$comments]);
	}

    public function store(Request $request,$id){
    	$this->validate($request , [			
			'content'=>'required',
			]);



		$posts=$request->user()->qccomments()->create([			
			'content'=>$request->input('content'),
			'qc_documents_id'=>$id,
			]);

		return response()->json(['message'=>'berhasil menambah komentar']);
    }
}
