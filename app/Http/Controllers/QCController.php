<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\QualityControl;

class QCController extends Controller
{
	public function index($id_parent){

		if($id_parent>0){
			$qc=QualityControl::findOrFail($id_parent);
			if($qc->hasChilds()){
				$qc=QualityControl::where('parent_id',$id_parent)->orderBy('id','asc')->get();
			}else{
				$qc='tidak ada';
			}
		}else{
			$qc=QualityControl::where('parent_id',$id_parent)->orderBy('id','asc')->get();
		}			


		return response()->json(['result'=>$qc]);
	}

	public function store(Request $request,$id_parent){

		$this->validate($request,
			[
			'name'=>'required|string|max:255'
			]);
		
		$qc=new QualityControl();

		$qc->name=$request->input('name');
		$qc->parent_id=$id_parent;

		$qc->save();
		return response()->json(['message'=>'berhasil menambah data'], 201);
	}
}
