<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class QCComment extends Model
{
	protected $table='qccomments';

	protected $fillable=[
	'content','qc_documents_id',
	];

	public function author(){
		return $this->belongsTo('App\Models\User','user_id');
	}

	public function document(){
		return $this->belongsTo('App\Models\QCDocument');
	}

	public function getCreatedAtAttribute()
	{
		return \Carbon\Carbon::parse($this->attributes['created_at'])->format('d M Y H:i');
	}
}
