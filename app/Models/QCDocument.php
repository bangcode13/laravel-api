<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class QCDocument extends Model
{
    protected $table='qc_documents';

    protected $fillable=[
    	'users_id','quality_controls_id','title','filename','content'
    ];


    public function job(){
    	return $this->belongsTo('App\Models\QualityControl');
    }

    public function author(){
    	return $this->belongsTo('App\Models\User','users_id','id');
    }

    public function qccomments(){
        return $this->hasMany('App\Models\Comment');
    }

}
