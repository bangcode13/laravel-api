<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class QualityControl extends Model
{
	protected $table='quality_controls';

	protected $fillable=[
		'name','parent_id'
	];

	public function childs(){
		return $this->hasMany('App\Models\QualityControl','parent_id');
	}

	public function parent(){
		return $this->belongsTo('App\Models\QualityControl','parent_id');
	}

	public function hasChilds(){
		return $this->childs()->count() > 0;
	}

	public function hasParent(){
		return $this->parent_id > 0;
	}

	public function documents(){
		return $this->hasMany('App\Models\QCDocument');
	}
}
