<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name','username','role', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


     public function posts(){
        return $this->hasMany('App\Models\Post');
    }

    public function qccomments(){
        return $this->hasMany('App\Models\QCComment');
    }

    public function documents(){
        return $this->hasMany('App\Models\QCDocument');
    }
}
