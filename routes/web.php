<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Route::get('/register', function () {
    return view('auth.register');
});

Route::get('/list-post', function(){
	return view('post.index');
});

Route::get('/addpost', function(){
	return view('post.create');
});

Route::get('showpost/{id}',function($id){
	return view('post.detail',compact('id'));
});

// Route::get('/gambar/{id}','GambarController@show');

// Route::post('upload/gambar','GambarController@store');
Auth::routes();

Route::get('/home', 'HomeController@index');

Route::get('komentar','QualityControl\QCCommentController@index');

Route::get('isi_konten','QCDocumentController@isikonten');
