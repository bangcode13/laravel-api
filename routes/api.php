<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::get('/user', function (Request $request) {
//     return $request->user();
// })->middleware('auth:api');

Route::group(['middleware' => ['api']],function(){
	
	Route::post('/auth/signin','AuthController@signin');
	


	Route::group(['middleware'=>['jwt.auth']], function(){
		Route::get('/profile','UserController@show');		

		Route::post('/auth/signup','AuthController@signup');
		//postingan
		// Route::get('/posts','PostController@index');
		// Route::post('/addpost','PostController@store');
		// Route::get('/post/{id}','PostController@show');		
		// Route::put('/posts/{id}','PostController@update');		
		// Route::delete('/posts/{id}','PostController@destroy');

		//komentar
		// Route::post('/comment/{id_post}','CommentController@store');
		// token
		Route::get('token','AuthController@getToken');
		//qc
		Route::post('/add/document/{qc}','QCDocumentController@store');

		Route::post('/add/qc/{parent}','QCController@store');
		Route::get('/list-qc/{parent}','QCController@index');

		Route::get('/list-document/{qc}','QCDocumentController@index');
		Route::get('/document-details/{id}','QCDocumentController@show');
		
		//komentar qd
		Route::post('/add-comments/{document_id}','QualityControl\QCCommentController@store');
		Route::get('/list-comments/{document_id}','QualityControl\QCCommentController@index');
		
	});

});
