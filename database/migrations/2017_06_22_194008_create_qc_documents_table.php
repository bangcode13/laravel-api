<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQcDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('qc_documents', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('users_id')->unsigned();
            $table->integer('quality_controls_id')->unsigned();
            $table->string('title');
            $table->string('filename');
            $table->timestamps();

           $table->foreign('users_id')->references('id')->on('users');
           $table->foreign('quality_controls_id')->references('id')->on('quality_controls');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('qc_documents');
    }
}
