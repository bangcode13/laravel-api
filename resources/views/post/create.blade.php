<!DOCTYPE html>
<html>
<head>
	<title>create post</title>
</head>
<body>
	<h2>Tambah Post</h2>
	<input type="title" name="title" placeholder="Judul ..." id="title"><br>
	<select name="category" id="category">
		<option value="Shop Drawing">Shop Drawing</option>
		<option value="Progress">Progress</option>
		<option value="Quality Control">QC</option> 
	</select><br>
	<textarea id="content" name="content" rows="8" cols="50"></textarea><br>

	<input type="submit" name="submit" value="Tambah Post" id="addpost">

	@include('auth.partial.asset')
	<script type="text/javascript">
		$(document).ready(function(){
			base_url = "http://localhost:8000/api/";

			$(document).on('click', '#addpost', function(){
				$.ajax({
					type: "POST",
					url : base_url + "addpost",
					contentType : "application/json",
					data: JSON.stringify({
						title : $('#title').val(),
						category : $('#category').val(),
						content : $('#content').val(),
					}),
					headers: {
						"Authorization" : 'Bearer ' + $.cookie('token')
					}
				}).done(function(response){
					window.location.href = "http://localhost:8000/list-post/";
				});
			});
		});	
	</script>
</body>
</html>