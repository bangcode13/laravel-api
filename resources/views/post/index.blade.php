<!DOCTYPE html>
<html>
<head>
	<title>List Post</title>
</head>
<body>
	<h1>List Post</h1>

	<div id="list_post">
		
	</div>
</body>

@include('auth.partial.asset')

<script type="text/javascript">

	$(document).ready(function(){
		base_url = "http://localhost:8000/api/";

		$.ajax({
			type: "GET",
			url : base_url + "posts",
			dataType : "json",
			headers: {
				"Authorization" : 'Bearer ' + $.cookie('token')
			}

		}).done(function(response){
				$.each(response, function(i){
					$('#list_post').append(
						"<div>"
							+"<h3>"+response[i].title+"</h3>"
							+"<span>"+response[i].user.name+"</span><br>"
							+"<span>"+response[i].category+"</span>"
							+"<p>"+response[i].content.substring(0,30)+"</p>"
							+
						"</div><br><br>"

						);
				});
		});
	});



</script>
</html>