<!DOCTYPE html>
<html>
<head>
	<title>detail post</title>
</head>
<body>
	<h1>Detail Post</h1>

	<input type="hidden" name="id_post" id="id_post" value="{{ $id }}">
	<div id="post"></div>

	@include('auth.partial.asset')

	<script type="text/javascript">
		$(document).ready(function(){
			base_url = "http://localhost:8000/api/";			

			$.ajax({
				type: "GET",
				url : base_url + "post/"+ $('#id_post').val(),
				headers: {
					"Authorization" : 'Bearer ' + $.cookie('token')
				}
			}).done(function(response){			
					console.log(response);
					$('#post').append(
						"<div>"
							+"<h3>"+response.title+"</h3>"							
							+"<span>"+response.category+"</span>"
							+"<p>"+response.content+"</p>"
							+
						"</div><br><br>"

						);
				
			});
		});

		
	</script>
</body>
</html>