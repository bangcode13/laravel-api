<!DOCTYPE html>
<html>
<head>
	<title>Login</title>
</head>
<body>
	<h1>Register</h1>

	<input type="name" name="name" placeholder="Name..." id="name"><br>
	<input type="username" name="username" placeholder="Username..." id="username"><br>
	<input type="email" name="email" placeholder="Email..." id="email"><br>
	<input type="password" name="password" placeholder="Password..." id="password"><br>
	<input type="submit" name="submit" value="Register" id="register">
	<a href="{{ url('/') }}">Login</a>

	@include('auth.partial.asset')

	<script type="text/javascript">

		$(document).ready(function(){
			base_url = "http://localhost:8000/api/";
			
			$(document).on('click', '#register', function(){
				$.ajax({
					type: "POST",
					url : base_url + "auth/signup", 
					
					contentType : "application/json",
					data: JSON.stringify({
						name: $('#name').val(),
						username: $('#username').val(),
						email: $('#email').val(),
						password: $('#password').val(),

					})
				}).done(function(response){
					console.log(response);

					alert('berhasil mendaftar');
				});
			});

		});

	</script>
</body>
</html>